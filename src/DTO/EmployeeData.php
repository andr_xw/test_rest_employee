<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class EmployeeData
{
    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $name;

    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $position;

    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $city;

    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $date;

    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $salary;

    /**
     * @param string $name
     * @param string $position
     * @param string $city
     * @param string $date
     * @param string $salary
     */
    public function __construct(
        string $name,
        string $position,
        string $city,
        string $date,
        string $salary
    ) {
        $this->name = $name;
        $this->position = $position;
        $this->city = $city;
        $this->date = $date;
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getSalary(): string
    {
        return $this->salary;
    }
}
