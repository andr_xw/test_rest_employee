<?php

namespace App\DTO;

class EmployeeFilter
{
    /**
     * @var string
     */
    private $name;

    /**
     * @param $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getFilterFields(): array
    {
        return array_filter(
            [
                'name' => $this->name,
            ]
        );
    }
}
