<?php

namespace App\DTO;

class SortParam
{
    /**
     * @var string (asc и не asc)
     */
    private $direction;

    /**
     * @var int
     */
    private $priority;

    /**
     * @param string $direction
     * @param int    $priority
     */
    public function __construct(string $direction, int $priority)
    {
        $this->direction = $direction;
        $this->priority = $priority;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }
}
