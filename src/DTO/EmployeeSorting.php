<?php

namespace App\DTO;

use App\Exception\SortingException;

class EmployeeSorting
{
    /**
     * @var SortParam
     */
    private $name;

    /**
     * @var SortParam
     */
    private $position;

    /**
     * @var SortParam
     */
    private $city;

    /**
     * @var SortParam
     */
    private $n;

    /**
     * @var SortParam
     */
    private $date;

    /**
     * @var SortParam
     */
    private $salary;

    /**
     * @return array
     */
    public function getSortingFields(): array
    {
        $sorting = array_filter(
            [
                'name' => $this->name,
                'position' => $this->position,
                'city' => $this->city,
                'n' => $this->n,
                'date' => $this->date,
                'salary' => $this->salary,
            ]
        );

        uasort(
            $sorting,
            function (SortParam $prev, SortParam $next): int {
                return $prev->getPriority() <=> $next->getPriority();
            }
        );

        return array_map(
            function (SortParam $sortParam): string {
                return $sortParam->getDirection();
            },
            $sorting
        );
    }

    /**
     * @param string    $fieldName
     * @param SortParam $sortParam
     *
     * @throws SortingException
     */
    public function set(string $fieldName, SortParam $sortParam): void
    {
        if (!property_exists($this, $fieldName)) {
            throw new SortingException("Field $fieldName not found");
        }

        $this->$fieldName = $sortParam;
    }
}
