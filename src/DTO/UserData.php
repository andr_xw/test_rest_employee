<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class UserData
{
    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $login;

    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $password;

    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var array | string[]
     */
    private $roles;

    /**
     * @param string $login
     * @param string $password
     * @param array  $roles
     */
    public function __construct(string $login, string $password, array $roles)
    {
        $this->login = $login;
        $this->password = $password;
        $this->roles = $roles;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return array | string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}
