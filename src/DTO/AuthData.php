<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class AuthData
{
    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $login;

    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     *
     * @var string
     */
    private $password;

    /**
     * @param string $login
     * @param string $password
     */
    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
