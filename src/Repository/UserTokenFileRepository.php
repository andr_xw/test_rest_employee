<?php

namespace App\Repository;

use App\Entity\UserToken;
use App\Exception\IOException;
use App\Exception\UserTokenNotFoundException;

class UserTokenFileRepository extends AbstractFileRepository implements UserTokenRepositoryInterface
{
    /**
     * @param array $filters
     * @param array $sorting
     *
     * @return array
     *
     * @throws IOException
     */
    public function getAll(array $filters = [], array $sorting = []): array
    {
        return array_map(
            function (array $userTokenData): UserToken {
                return $this->hydration($userTokenData, UserToken::class);
            },
            $this->sortContent($sorting, $this->filterContent($filters, $this->getContent()))
        );
    }

    /**
     * @param string $token
     *
     * @return UserToken
     *
     * @throws UserTokenNotFoundException
     * @throws IOException
     */
    public function getOneByToken(string $token): UserToken
    {
        foreach ($this->getContent() as $userTokenData) {
            if ($token == $userTokenData[$this->getPrimaryKeyColumn()]) {
                return $this->hydration($userTokenData, UserToken::class);
            }
        }

        throw new UserTokenNotFoundException("Token $token not found");
    }

    /**
     * @param UserToken $userToken
     *
     * @throws IOException
     */
    public function delete(UserToken $userToken): void
    {
        foreach ($this->getContent() as $index => $userTokenData) {
            if ($userToken->getToken() == $userTokenData[$this->getPrimaryKeyColumn()]) {
                $this->deleteByIndex($index);

                break;
            }
        }
    }

    /**
     * @param UserToken $userToken
     *
     * @throws IOException
     */
    public function add(UserToken $userToken): void
    {
        $this->addContent($userToken);
    }

    /**
     * @return string
     */
    protected function getStorageFileName(): string
    {
        return strtolower(substr(UserToken::class, strrpos(UserToken::class, '\\') + 1));
    }

    /**
     * @return string
     */
    protected function getPrimaryKeyColumn(): string
    {
        return 'token';
    }
}
