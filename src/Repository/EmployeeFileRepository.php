<?php

namespace App\Repository;

use App\Entity\Employee;
use App\Exception\EmployeeNotFound;
use App\Exception\IOException;

class EmployeeFileRepository extends AbstractFileRepository implements EmployeeRepositoryInterface
{
    /**
     * @param array $filters
     * @param array $sorting
     *
     * @return array
     *
     * @throws IOException
     */
    public function getAll(array $filters = [], array $sorting = []): array
    {
        return array_map(
            function (array $employeeData): Employee {
                return $this->hydration($employeeData, Employee::class);
            },
            $this->sortContent($sorting, $this->filterContent($filters, $this->getContent()))
        );
    }

    /**
     * @param int $number
     *
     * @return Employee
     *
     * @throws EmployeeNotFound
     * @throws IOException
     */
    public function getOneByNumber(int $number): Employee
    {
        foreach ($this->getContent() as $employeeData) {
            if ($number == $employeeData[$this->getPrimaryKeyColumn()]) {
                return $this->hydration($employeeData, Employee::class);
            }
        }

        throw new EmployeeNotFound("Employee $number not found");
    }

    /**
     * @param Employee $employee
     *
     * @throws IOException
     */
    public function delete(Employee $employee): void
    {
        foreach ($this->getContent() as $index => $employeeData) {
            if ($employee->getNumber() == $employeeData[$this->getPrimaryKeyColumn()]) {
                $this->deleteByIndex($index);

                break;
            }
        }
    }

    /**
     * @param Employee $employee
     *
     * @throws IOException
     */
    public function add(Employee $employee): void
    {
        $this->addContent($employee);
    }

    /**
     * @throws IOException
     */
    public function generateNextNumber(): int
    {
        return max(array_map('intval', array_column($this->getContent(), $this->getPrimaryKeyColumn()))) + 1;
    }

    /**
     * @return string
     */
    protected function getStorageFileName(): string
    {
        return strtolower(substr(Employee::class, strrpos(Employee::class, '\\') + 1));
    }

    /**
     * @return string
     */
    protected function getPrimaryKeyColumn(): string
    {
        return 'n';
    }
}
