<?php

namespace App\Repository;

use App\Exception\FilterException;
use App\Exception\IOException;
use App\StorageDriver\FileStorageDriver;
use JMS\Serializer\Serializer;

/**
 * Самый абстрактный из файловых репозиториев
 */
abstract class AbstractFileRepository
{
    /**
     * @var FileStorageDriver
     */
    private $fileStorageDriver;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @return string
     */
    abstract protected function getStorageFileName(): string;

    /**
     * @return string
     */
    abstract protected function getPrimaryKeyColumn(): string;

    /**
     * @param FileStorageDriver $fileStorageDriver
     * @param Serializer        $serializer
     */
    public function __construct(FileStorageDriver $fileStorageDriver, Serializer $serializer)
    {
        $this->fileStorageDriver = $fileStorageDriver;
        $this->serializer = $serializer;
    }

    /**
     * @param array  $data
     * @param string $type
     *
     * @return mixed
     */
    protected function hydration(array $data, string $type) {
        return $this->serializer->fromArray($data, $type);
    }

    /**
     * @param array $sorting
     * @param array $content
     *
     * @return array
     */
    protected function sortContent(array $sorting, array $content): array
    {
        foreach ($sorting as $field => $direction) {
            usort(
                $content,
                function (array $prev, array $next) use ($field, $direction): int {
                    return ('asc' === $direction) ? $prev[$field] <=> $next[$field] : $next[$field] <=> $prev[$field];
                }
            );
        }

        return $content;
    }

    /**
     * @param array $filters
     * @param array $content
     *
     * @return array
     */
    protected function filterContent(array $filters, array $content): array
    {
        return array_filter(
            $content,
            function (array $employeeData) use ($filters): bool {
                foreach ($filters as $filterName => $filterValue) {
                    if (!isset($employeeData[$filterName])) {
                        throw new FilterException("Field $filterName not found");
                    }

                    $haystack = strtolower($employeeData[$filterName]);

                    if (false === strpos($haystack, strtolower($filterValue))) {
                        return false;
                    }
                }

                return true;
            }
        );
    }

    /**
     * @param mixed $data
     *
     * @throws IOException
     */
    protected function addContent($data): void
    {
        $this->fileStorageDriver->addContent(
            $this->getStorageFileName(),
            $this->serializer->toArray($data)
        );
    }

    /**
     * @return array
     *
     * @throws IOException
     */
    protected function getContent(): array
    {
        return $this->fileStorageDriver->getContent($this->getStorageFileName());
    }

    /**
     * @param int $index
     *
     * @throws IOException
     */
    protected function deleteByIndex(int $index): void
    {
        $this->fileStorageDriver->deleteByIndex($this->getStorageFileName(), $index);
    }
}
