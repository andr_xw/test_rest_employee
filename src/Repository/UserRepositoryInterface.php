<?php

namespace App\Repository;

use App\Entity\User;
use App\Exception\UserNotFoundException;

interface UserRepositoryInterface
{
    /**
     * @param array $filters
     * @param array $sorting
     *
     * @return array | User[]
     */
    public function getAll(array $filters = [], array $sorting = []): array;

    /**
     * @param string $login
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function getOneByLogin(string $login): User;

    /**
     * @param User $user
     */
    public function delete(User $user): void;

    /**
     * @param User $user
     */
    public function add(User $user): void;
}
