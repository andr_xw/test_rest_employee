<?php

namespace App\Repository;

use App\Entity\UserToken;
use App\Exception\UserTokenNotFoundException;

interface UserTokenRepositoryInterface
{
    /**
     * @param array $filters
     * @param array $sorting
     *
     * @return array | UserToken[]
     */
    public function getAll(array $filters = [], array $sorting = []): array;

    /**
     * @param string $token
     *
     * @return UserToken
     *
     * @throws UserTokenNotFoundException
     */
    public function getOneByToken(string $token): UserToken;

    /**
     * @param UserToken $userToken
     */
    public function delete(UserToken $userToken): void;

    /**
     * @param UserToken $userToken
     */
    public function add(UserToken $userToken): void;
}
