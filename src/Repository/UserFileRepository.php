<?php

namespace App\Repository;

use App\Entity\User;
use App\Exception\IOException;
use App\Exception\UserNotFoundException;

class UserFileRepository extends AbstractFileRepository implements UserRepositoryInterface
{
    /**
     * @param array $filters
     * @param array $sorting
     *
     * @return array
     *
     * @throws IOException
     */
    public function getAll(array $filters = [], array $sorting = []): array
    {
        return array_map(
            function (array $userData): User {
                return $this->hydration($userData, User::class);
            },
            $this->sortContent($sorting, $this->filterContent($filters, $this->getContent()))
        );
    }

    /**
     * @param string $login
     *
     * @return User
     *
     * @throws UserNotFoundException
     * @throws IOException
     */
    public function getOneByLogin(string $login): User
    {
        foreach ($this->getContent() as $userData) {
            if ($login == $userData[$this->getPrimaryKeyColumn()]) {
                return $this->hydration($userData, User::class);
            }
        }

        throw new UserNotFoundException("User $login not found");
    }

    /**
     * @param User $user
     *
     * @throws IOException
     */
    public function delete(User $user): void
    {
        foreach ($this->getContent() as $index => $userData) {
            if ($user->getLogin() == $userData[$this->getPrimaryKeyColumn()]) {
                $this->deleteByIndex($index);

                break;
            }
        }
    }

    /**
     * @param User $user
     *
     * @throws IOException
     */
    public function add(User $user): void
    {
        $this->addContent($user);
    }

    /**
     * @return string
     */
    protected function getStorageFileName(): string
    {
        return strtolower(substr(User::class, strrpos(User::class, '\\') + 1));
    }

    /**
     * @return string
     */
    protected function getPrimaryKeyColumn(): string
    {
        return 'login';
    }
}
