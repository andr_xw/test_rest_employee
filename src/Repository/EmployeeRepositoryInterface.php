<?php

namespace App\Repository;

use App\Entity\Employee;
use App\Exception\EmployeeNotFound;

interface EmployeeRepositoryInterface
{
    /**
     * @param array $filters
     * @param array $sorting
     *
     * @return array | Employee[]
     */
    public function getAll(array $filters = [], array $sorting = []): array;

    /**
     * @param int $number
     *
     * @return Employee
     *
     * @throws EmployeeNotFound
     */
    public function getOneByNumber(int $number): Employee;

    /**
     * @param Employee $employee
     */
    public function delete(Employee $employee): void;

    /**
     * @param Employee $employee
     */
    public function add(Employee $employee): void;

    /**
     * @return int
     */
    public function generateNextNumber(): int;
}
