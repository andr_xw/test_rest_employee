<?php

namespace App\ArgumentResolver;

use App\DTO\EmployeeFilter;
use App\DTO\EmployeeSorting;
use App\DTO\SortParam;
use App\Exception\SortingException;
use Generator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class EmployeeSortingResolver implements ArgumentValueResolverInterface
{
    /**
     * @param Request          $request
     * @param ArgumentMetadata $argument
     *
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return EmployeeSorting::class === $argument->getType();
    }

    /**
     * @param Request          $request
     * @param ArgumentMetadata $argument
     *
     * @return Generator
     *
     * @throws SortingException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): Generator
    {
        // Выглядеть оно будет так:
        // [
        //     'sort' => [
        //         'n' => ['direction' => 'desc', 'priority' => 1],
        //         'city' => ['direction' => 'asc', 'priority' => 2],
        //     ],
        // ]
        $sort = $request->query->get('sort', []);

        $employeeSorting = new EmployeeSorting();

        foreach ($sort as $fieldName => $fieldParams) {
            $direction = $fieldParams['direction'] ?? 'asc';
            $priority = $fieldParams['priority'] ?? 0;

            $employeeSorting->set($fieldName, new SortParam($direction, $priority));
        }

        yield $employeeSorting;
    }
}
