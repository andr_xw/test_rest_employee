<?php

namespace App\ArgumentResolver;

use App\Exception\BearerTokenException;
use App\Factory\BearerTokenFactory;
use App\VO\BearerToken;
use Generator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BearerTokenResolver implements ArgumentValueResolverInterface
{
    /**
     * @var BearerTokenFactory
     */
    private $bearerTokenFactory;

    /**
     * @param BearerTokenFactory $bearerTokenFactory
     */
    public function __construct(BearerTokenFactory $bearerTokenFactory)
    {
        $this->bearerTokenFactory = $bearerTokenFactory;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     *
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return BearerToken::class === $argument->getType();
    }

    /**
     * @param Request          $request
     * @param ArgumentMetadata $argument
     *
     * @return Generator
     *
     * @throws BadRequestHttpException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): Generator
    {
        try {
            yield $this->bearerTokenFactory->createFromRequest($request);
        } catch (BearerTokenException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }
    }
}
