<?php

namespace App\Controller;

use App\DTO\UserData;
use App\Entity\User;
use App\Exception\IOException;
use App\Exception\UserNotFoundException;
use App\Exception\ValidationHttpException;
use App\Service\UserService;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param UserService        $userService
     * @param ValidatorInterface $validator
     */
    public function __construct(UserService $userService, ValidatorInterface $validator)
    {
        $this->userService = $userService;
        $this->validator = $validator;
    }

    /**
     * @Rest\Post("/users")
     *
     * @param UserData $userData
     *
     * @return View
     *
     * @throws IOException
     * @throws ValidationHttpException
     */
    public function create(UserData $userData): View
    {
        $errors = $this->validator->validate($userData);

        if (!empty(count($errors))) {
            throw new ValidationHttpException('Incorrect user data', $errors);
        }

        $user = $this->userService->create(
            $userData->getLogin(),
            $userData->getPassword(),
            $userData->getRoles()
        );

        return View::create(
            $user,
            Response::HTTP_CREATED,
            ['location' => "/users/{$user->getLogin()}"]
        );
    }

    /**
     * @Rest\Delete("/users/{number}")
     *
     * @param string $login
     *
     * @return View
     *
     * @throws NotFoundHttpException
     * @throws IOException
     */
    public function delete(string $login): View
    {
        try {
            $this->userService->delete($login);
        } catch (UserNotFoundException $exception) {
            throw new NotFoundHttpException($exception->getMessage(), $exception);
        }

        return View::create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Get("/users")
     *
     * @return View
     */
    public function getAll(): View
    {
        return View::create($this->userService->getAll(), Response::HTTP_OK)
            ->setContext((new Context())->addGroup('View'));
    }

    /**
     * @Rest\Get("/users/{number}")
     *
     * @param string $login
     *
     * @return User
     *
     * @throws NotFoundHttpException
     */
    public function getOneByLogin(string $login): User
    {
        try {
            return $this->userService->getOneByLogin($login);
        } catch (UserNotFoundException $exception) {
            throw new NotFoundHttpException($exception->getMessage(), $exception);
        }
    }
}
