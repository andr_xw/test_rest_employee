<?php

namespace App\Controller;

use App\DTO\AuthData;
use App\Exception\AuthException;
use App\Exception\IOException;
use App\Exception\ValidationHttpException;
use App\Service\AuthService;
use App\VO\BearerToken;
use Exception;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthController
{
    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param AuthService $authService
     * @param ValidatorInterface $validator
     */
    public function __construct(AuthService $authService, ValidatorInterface $validator)
    {
        $this->authService = $authService;
        $this->validator = $validator;
    }

    /**
     * @Rest\Post("/auth/login")
     *
     * @param AuthData $authData
     *
     * @return View
     *
     * @throws ValidationHttpException
     * @throws BadRequestHttpException
     * @throws Exception
     */
    public function login(AuthData $authData): View
    {
        $errors = $this->validator->validate($authData);

        if (!empty(count($errors))) {
            throw new ValidationHttpException('Incorrect auth data', $errors);
        }

        try {
            $authToken = $this->authService->login($authData->getLogin(), $authData->getPassword());
        } catch (AuthException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }

        return View::create($authToken, Response::HTTP_CREATED)
            ->setContext((new Context())->addGroup("ViewToken"));
    }

    /**
     * @Rest\Get("/auth/logout")
     *
     * @param BearerToken $bearerToken
     *
     * @return View
     *
     * @throws IOException
     */
    public function logout(BearerToken $bearerToken): View
    {
        $this->authService->logout($bearerToken);

        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}
