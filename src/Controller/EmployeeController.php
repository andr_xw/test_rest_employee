<?php

namespace App\Controller;

use App\DTO\EmployeeData;
use App\DTO\EmployeeFilter;
use App\DTO\EmployeeSorting;
use App\Entity\Employee;
use App\Exception\EmployeeNotFound;
use App\Exception\IOException;
use App\Exception\ValidationHttpException;
use App\Service\EmployeeService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EmployeeController
{
    /**
     * @var EmployeeService
     */
    private $employeeService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param EmployeeService    $employeeService
     * @param ValidatorInterface $validator
     */
    public function __construct(EmployeeService $employeeService, ValidatorInterface $validator)
    {
        $this->employeeService = $employeeService;
        $this->validator = $validator;
    }

    /**
     * @Rest\Post("/employees")
     *
     * @param EmployeeData $employeeData
     *
     * @return View
     *
     * @throws IOException
     * @throws ValidationHttpException
     */
    public function create(EmployeeData $employeeData): View
    {
        $errors = $this->validator->validate($employeeData);

        if (!empty(count($errors))) {
            throw new ValidationHttpException('Incorrect employee data', $errors);
        }

        $employee = $this->employeeService->create(
            $employeeData->getName(),
            $employeeData->getPosition(),
            $employeeData->getCity(),
            $employeeData->getDate(),
            $employeeData->getSalary()
        );

        return View::create(
            $employee,
            Response::HTTP_CREATED,
            ['location' => "/employees/{$employee->getNumber()}"]
        );
    }

    /**
     * @Rest\Put("/employees/{number}")
     *
     * @param int          $number
     * @param EmployeeData $employeeData
     *
     * @return Employee
     *
     * @throws IOException
     * @throws ValidationHttpException
     * @throws NotFoundHttpException
     */
    public function update(int $number, EmployeeData $employeeData): Employee
    {
        $errors = $this->validator->validate($employeeData);

        if (!empty(count($errors))) {
            throw new ValidationHttpException('Incorrect employee data', $errors);
        }

        try {
            return $this->employeeService->update(
                $employeeData->getName(),
                $employeeData->getPosition(),
                $employeeData->getCity(),
                $number,
                $employeeData->getDate(),
                $employeeData->getSalary()
            );
        } catch (EmployeeNotFound $exception) {
            throw new NotFoundHttpException($exception->getMessage(), $exception);
        }
    }

    /**
     * @Rest\Delete("/employees/{number}")
     *
     * @param int $number
     *
     * @return View
     *
     * @throws NotFoundHttpException
     * @throws IOException
     */
    public function delete(int $number): View
    {
        try {
            $this->employeeService->delete($number);
        } catch (EmployeeNotFound $exception) {
            throw new NotFoundHttpException($exception->getMessage(), $exception);
        }

        return View::create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Get("/employees")
     *
     * @param EmployeeFilter  $employeeFilter
     * @param EmployeeSorting $employeeSorting
     *
     * @return array | Employee[]
     */
    public function getAll(EmployeeFilter $employeeFilter, EmployeeSorting $employeeSorting): array
    {
        return $this->employeeService->getAll($employeeFilter, $employeeSorting);
    }

    /**
     * @Rest\Get("/employees/{number}")
     *
     * @param int $number
     *
     * @return Employee
     *
     * @throws NotFoundHttpException
     */
    public function getOneByNumber(int $number): Employee
    {
        try {
            return $this->employeeService->getOneByNumber($number);
        } catch (EmployeeNotFound $exception) {
            throw new NotFoundHttpException($exception->getMessage(), $exception);
        }
    }
}
