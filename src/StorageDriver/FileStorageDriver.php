<?php

namespace App\StorageDriver;

use App\Exception\IOException;

class FileStorageDriver
{
    /**
     * @var string
     */
    private $dirPath;

    /**
     * @var array
     */
    private $content = [];

    /**
     * @var array
     */
    private $head;

    /**
     * @var array
     */
    private $headIndex;

    /**
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->dirPath = $filePath;
    }

    /**
     * @param string $fileName
     *
     * @return array
     *
     * @throws IOException
     */
    public function getContent(string $fileName): array
    {
        if (false === $this->isContentLoaded($fileName)) {
            $this->loadContent($fileName);
        }

        return $this->content[$fileName];
    }

    /**
     * @param string $fileName
     * @param int    $index
     *
     * @throws IOException
     */
    public function deleteByIndex(string $fileName, int $index): void
    {
        if (false === $this->isContentLoaded($fileName)) {
            $this->loadContent($fileName);
        }

        unset($this->content[$fileName][$index]);
    }

    /**
     * @param string $fileName
     * @param array  $data
     *
     * @throws IOException
     */
    public function addContent(string $fileName, array $data): void
    {
        if (false === $this->isContentLoaded($fileName)) {
            $this->loadContent($fileName);
        }

        $this->content[$fileName][] = $data;
    }

    /**
     * @throws IOException
     */
    public function commit(): void
    {
        foreach ($this->content as $fileName => $content) {
            $headIndex = $this->headIndex[$fileName];
            $head = $this->headIndex[$fileName];

            $sizes = $this->getColumnSizes($fileName);

            $preHead = array_map(
                function (string $value, string $key) use ($sizes): string {
                    return str_pad($value, $sizes[$key], ' ', STR_PAD_RIGHT);
                },
                $head,
                $headIndex
            );

            $preContent = array_map(
                function (array $record) use ($sizes): array {
                    return array_map(
                        function (string $value, string $key) use ($sizes): string {
                            return str_pad($value, $sizes[$key], ' ', STR_PAD_RIGHT);
                        },
                        $record,
                        array_keys($record)
                    );
                },
                $content
            );

            $preDelimiter = array_map(
                function (int $columnSize): string {
                    return str_pad('', $columnSize, '-');
                },
                $sizes
            );

            $this->saveFile($fileName, $preContent, $preHead, $preDelimiter);
        }
    }

    /**
     * @param string $fileName
     *
     * @return bool
     */
    private function isContentLoaded(string $fileName): bool
    {
        return isset($this->content[$fileName]);
    }

    /**
     * @param string $fileName
     *
     * @throws IOException
     */
    private function loadContent(string $fileName): void
    {
        $filePath = $this->getFilePath($fileName);

        $sourceContent = file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        if (false === $sourceContent) {
            throw new IOException("Не удалось прочитать файл $filePath");
        }

        $cleanContent = array_map(
            function (string $row): array {
                return array_map('trim', explode('|', preg_replace('/\|(.+)\|/u', '$1', $row)));
            },
            array_filter(
                $sourceContent,
                function (string $row): bool {
                    return (bool) preg_match('/[^-+]+/', $row);
                }
            )
        );

        $head =  array_shift($cleanContent);
        $headIndex = array_map('strtolower', $head);

        $this->headIndex[$fileName] = $headIndex;
        $this->head[$fileName] = array_combine($headIndex, $head);

        $this->content[$fileName] = array_map(
            function (array $record) use ($fileName): array {
                return array_combine($this->headIndex[$fileName], $record);
            },
            $cleanContent
        );
    }

    /**
     * @param string $fileName
     *
     * @return array
     */
    private function getColumnSizes(string $fileName): array
    {
        $content = array_merge([$this->head[$fileName]], $this->content[$fileName]);
        $columnNames = $this->headIndex[$fileName];

        return array_map(
            function (string $columnName) use ($content): int {
                return max(
                    array_map(
                        function (string $value): int {
                            return strlen($value);
                        },
                        array_column($content, $columnName)
                    )
                );
            },
            // Используем combine, чтобы итоговый массив имел ассоциативные ключи
            array_combine($columnNames, $columnNames)
        );
    }

    /**
     * @param string $fileName
     * @param array  $preContent
     * @param array  $preHead
     * @param array  $preDelimiter
     *
     * @throws IOException
     */
    private function saveFile(string $fileName, array $preContent, array $preHead, array $preDelimiter): void
    {
        $delimiter = '+-' . implode('-+-', $preDelimiter) . '-+';

        $data = [$delimiter];

        $data[] = '| ' . implode(' | ', $preHead) . ' |';;
        $data[] = $delimiter;

        foreach ($preContent as $line) {
            $data[] = '| ' . implode(' | ', $line) . ' |';
            $data[] = $delimiter;
        }

        $filePath = $this->getFilePath($fileName);

        $result = file_put_contents($filePath, implode("\n", $data));

        if (false === $result) {
            throw new IOException("Could not write file $filePath");
        }
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function getFilePath(string $fileName): string {
        return "$this->dirPath/$fileName.txt";
    }
}
