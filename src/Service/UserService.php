<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\UserNotFoundException;
use App\Exception\IOException;
use App\Repository\UserRepositoryInterface;
use App\StorageDriver\FileStorageDriver;
use App\VO\PasswordHash;

class UserService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var FileStorageDriver
     */
    private $fileStorageDriver;

    /**
     * @param UserRepositoryInterface $employeeRepository
     * @param FileStorageDriver       $fileStorageDriver
     */
    public function __construct(UserRepositoryInterface $employeeRepository, FileStorageDriver $fileStorageDriver)
    {
        $this->userRepository = $employeeRepository;
        $this->fileStorageDriver = $fileStorageDriver;
    }

    /**
     * @param string $login
     * @param string $password
     * @param array  $roles
     *
     * @return User
     *
     * @throws IOException
     */
    public function create(string $login, string $password, array $roles): User {
        $user = new User($login, (string) PasswordHash::createFromPassword($password), $roles);

        $this->userRepository->add($user);

        $this->fileStorageDriver->commit();

        return $user;
    }

    /**
     * @param string $login
     *
     * @throws UserNotFoundException
     * @throws IOException
     */
    public function delete(string $login): void
    {
        $this->userRepository->delete(
            $this->getOneByLogin($login)
        );

        $this->fileStorageDriver->commit();
    }

    /**
     * @return array | User[]
     */
    public function getAll(): array
    {
        return $this->userRepository->getAll();
    }

    /**
     * @param string $login
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function getOneByLogin(string $login): User
    {
        return $this->userRepository->getOneByLogin($login);
    }
}
