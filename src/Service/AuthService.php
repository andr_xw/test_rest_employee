<?php

namespace App\Service;

use App\Entity\UserToken;
use App\Exception\AuthException;
use App\Exception\GenerateAuthTokenException;
use App\Exception\IOException;
use App\Exception\UserNotFoundException;
use App\Exception\UserTokenNotFoundException;
use App\Repository\UserRepositoryInterface;
use App\Repository\UserTokenRepositoryInterface;
use App\StorageDriver\FileStorageDriver;
use App\VO\BearerToken;
use App\VO\PasswordHash;
use Exception;

class AuthService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var UserTokenRepositoryInterface
     */
    private $userTokenRepository;

    /**
     * @var FileStorageDriver
     */
    private $fileStorageDriver;

    /**
     * @param UserRepositoryInterface      $userRepository
     * @param UserTokenRepositoryInterface $userTokenRepository
     * @param FileStorageDriver            $fileStorageDriver
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserTokenRepositoryInterface $userTokenRepository,
        FileStorageDriver $fileStorageDriver
    ) {
        $this->userRepository = $userRepository;
        $this->userTokenRepository = $userTokenRepository;
        $this->fileStorageDriver = $fileStorageDriver;
    }

    /**
     * @param string $login
     * @param string $password
     *
     * @return UserToken
     *
     * @throws AuthException
     * @throws GenerateAuthTokenException
     * @throws IOException
     * @throws UserNotFoundException
     */
    public function login(string $login, string $password): UserToken
    {
        $user = $this->userRepository->getOneByLogin($login);
        $passwordHash = new PasswordHash($user->getPasswordHash());

        if (!$passwordHash->verifyPassword($password)) {
            throw new AuthException('Invalid login or password');
        }

        $userToken = new UserToken($this->createToken(), $user->getLogin());

        $this->userTokenRepository->add($userToken);
        $this->fileStorageDriver->commit();

        return $userToken;
    }

    /**
     * @param BearerToken $bearerToken
     *
     * @throws IOException
     */
    public function logout(BearerToken $bearerToken): void
    {
        try {
            $this->userTokenRepository->delete(
                $this->userTokenRepository->getOneByToken(
                    $bearerToken->getValue()
                )
            );

            $this->fileStorageDriver->commit();
        } catch (UserTokenNotFoundException $exception) {
            // Ну рас его нет, то и удалять не надо))
        }
    }

    /**
     * @return string
     *
     * @throws GenerateAuthTokenException
     */
    private function createToken(): string
    {
        try {
            return hash('sha256', random_bytes(rand(50, 80)));
        } catch (Exception $exception) {
            throw new GenerateAuthTokenException('Could not generate authorization token', 0, $exception);
        }
    }
}
