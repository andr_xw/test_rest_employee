<?php

namespace App\Service;

use App\DTO\EmployeeFilter;
use App\DTO\EmployeeSorting;
use App\Entity\Employee;
use App\Exception\EmployeeNotFound;
use App\Exception\IOException;
use App\Repository\EmployeeRepositoryInterface;
use App\StorageDriver\FileStorageDriver;

class EmployeeService
{
    /**
     * @var EmployeeRepositoryInterface
     */
    private $employeeRepository;

    /**
     * @var FileStorageDriver
     */
    private $fileStorageDriver;

    /**
     * @param EmployeeRepositoryInterface $employeeRepository
     * @param FileStorageDriver           $fileStorageDriver
     */
    public function __construct(EmployeeRepositoryInterface $employeeRepository, FileStorageDriver $fileStorageDriver)
    {
        $this->employeeRepository = $employeeRepository;
        $this->fileStorageDriver = $fileStorageDriver;
    }

    /**
     * @param string $name
     * @param string $position
     * @param string $city
     * @param string $date
     * @param string $salary
     *
     * @return Employee
     *
     * @throws IOException
     */
    public function create(
        string $name,
        string $position,
        string $city,
        string $date,
        string $salary
    ): Employee {
        $number = $this->employeeRepository->generateNextNumber();

        $employee = new Employee($name, $position, $city, $number, $date, $salary);

        $this->employeeRepository->add($employee);

        $this->fileStorageDriver->commit();

        return $employee;
    }

    /**
     * TODO: Обновление работать не будет, ввиду отсутствия identity map и unit of work,
     * TODO: но писать их мне лень, а делать из репозитория DAO религия не позволяет
     *
     * @param string $name
     * @param string $position
     * @param string $city
     * @param int    $number
     * @param string $date
     * @param string $salary
     *
     * @return Employee
     *
     * @throws EmployeeNotFound
     * @throws IOException
     */
    public function update(
        string $name,
        string $position,
        string $city,
        int $number,
        string $date,
        string $salary
    ): Employee {
        $employee = $this->getOneByNumber($number);
        $employee->edit($name, $position, $city, $date, $salary);

        $this->fileStorageDriver->commit();

        return $employee;
    }

    /**
     * @param int $number
     *
     * @throws EmployeeNotFound
     * @throws IOException
     */
    public function delete(int $number): void
    {
        $this->employeeRepository->delete(
            $this->getOneByNumber($number)
        );

        $this->fileStorageDriver->commit();
    }

    /**
     * @param EmployeeFilter  $employeeFilter
     * @param EmployeeSorting $employeeSorting
     *
     * @return array | Employee[]
     */
    public function getAll(EmployeeFilter $employeeFilter, EmployeeSorting $employeeSorting): array
    {
        return $this->employeeRepository->getAll(
            $employeeFilter->getFilterFields(),
            $employeeSorting->getSortingFields()
        );
    }

    /**
     * @param int $number
     *
     * @return Employee
     *
     * @throws EmployeeNotFound
     */
    public function getOneByNumber(int $number): Employee
    {
        return $this->employeeRepository->getOneByNumber($number);
    }
}
