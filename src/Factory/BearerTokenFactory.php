<?php

namespace App\Factory;

use App\Exception\BearerTokenException;
use App\VO\BearerToken;
use Symfony\Component\HttpFoundation\Request;

class BearerTokenFactory
{
    /**
     * @param Request $request
     *
     * @return BearerToken
     *
     * @throws BearerTokenException
     */
    public function createFromRequest(Request $request): BearerToken
    {
        $authHeader = $request->headers->get('authorization');

        if (empty($authHeader)) {
            throw new BearerTokenException('Requires http-header "Authorization"');
        }

        $authData = sscanf($authHeader, '%s %s');

        if (count($authData) !== 2 || empty($authData[0]) || empty($authData[1])) {
            throw new BearerTokenException('Invalid http-header "Authorization"');
        }

        [$authType, $authToken] = $authData;

        if ('Bearer' !== $authType) {
            throw new BearerTokenException('Requires "Bearer authorization"');
        }

        return new BearerToken($authToken);
    }
}
