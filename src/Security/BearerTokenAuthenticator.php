<?php

namespace App\Security;

use App\Exception\BearerTokenException;
use App\Exception\UserTokenNotFoundException;
use App\Factory\BearerTokenFactory;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

class BearerTokenAuthenticator implements SimplePreAuthenticatorInterface
{
    /**
     * @var BearerTokenFactory
     */
    private $bearerTokenFactory;

    /**
     * @param BearerTokenFactory $bearerTokenFactory
     */
    public function __construct(BearerTokenFactory $bearerTokenFactory)
    {
        $this->bearerTokenFactory = $bearerTokenFactory;
    }

    /**
     * @param Request $request
     * @param string  $providerKey
     *
     * @return PreAuthenticatedToken
     *
     * @throws InvalidArgumentException
     * @throws BadRequestHttpException
     */
    public function createToken(Request $request, $providerKey): PreAuthenticatedToken
    {
        try {
            $authToken = $this->bearerTokenFactory->createFromRequest($request);
        } catch (BearerTokenException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }

        return new PreAuthenticatedToken('anon.', $authToken, $providerKey);
    }

    /**
     * @param TokenInterface $token
     * @param string         $providerKey
     *
     * @return bool
     */
    public function supportsToken(TokenInterface $token, $providerKey): bool
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * @param TokenInterface        $token
     * @param UserProviderInterface $userProvider
     * @param string                $providerKey
     *
     * @return PreAuthenticatedToken
     *
     * @throws InvalidArgumentException
     * @throws UsernameNotFoundException
     * @throws AccessDeniedHttpException
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof BearerTokenUserProvider) {
            throw new InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of ApiKeyUserProvider (%s was given).',
                    get_class($userProvider)
                )
            );
        }

        $authToken = $token->getCredentials();

        try {
            $username = $userProvider->getUsernameForApiKey($authToken);
        } catch (UserTokenNotFoundException $exception) {
            throw new AccessDeniedHttpException($exception->getMessage(), $exception);
        }

        $user = $userProvider->loadUserByUsername($username);

        return new PreAuthenticatedToken($user, $authToken, $providerKey, $user->getRoles());
    }
}
