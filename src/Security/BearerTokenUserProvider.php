<?php

namespace App\Security;

use App\Exception\UserTokenNotFoundException;
use App\Repository\UserTokenRepositoryInterface;
use InvalidArgumentException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class BearerTokenUserProvider implements UserProviderInterface
{
    /**
     * @var UserTokenRepositoryInterface
     */
    private $userTokenRepository;

    /**
     * @param UserTokenRepositoryInterface $userTokenRepository
     */
    public function __construct(UserTokenRepositoryInterface $userTokenRepository)
    {
        $this->userTokenRepository = $userTokenRepository;
    }

    /**
     * @param string $authToken
     *
     * @return string
     *
     * @throws UserTokenNotFoundException
     */
    public function getUsernameForApiKey(string $authToken): string
    {
        return $this->userTokenRepository->getOneByToken($authToken)
            ->getToken();
    }

    /**
     * @param string $username
     *
     * @return User
     *
     * @throws InvalidArgumentException
     */
    public function loadUserByUsername($username): User
    {
        return new User(
            $username,
            null,
            ['ROLE_USER']
        );
    }

    /**
     * @param UserInterface $user
     *
     * @throws UnsupportedUserException
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class): bool
    {
        return User::class === $class;
    }
}
