<?php

namespace App\Entity;

use JMS\Serializer\Annotation as Serializer;

class UserToken
{
    /**
     * @Serializer\Groups({"ViewToken"})
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $token;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $login;

    /**
     * @param string $token
     * @param string $login
     */
    public function __construct(string $token, string $login)
    {
        $this->token = $token;
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }
}
