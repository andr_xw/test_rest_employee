<?php

namespace App\Entity;

use JMS\Serializer\Annotation as Serializer;

class User
{
    /**
     * @Serializer\Groups({"Default", "View"})
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $login;

    /**
     * @Serializer\Groups({"Default"})
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $passwordHash;

    /**
     * @Serializer\Groups({"Default", "View"})
     * @Serializer\Type("string")
     *
     * @var array | string[]
     */
    private $roles;

    /**
     * @param string           $login
     * @param string           $passwordHash
     * @param array | string[] $roles
     */
    public function __construct(string $login, string $passwordHash, array $roles)
    {
        $this->login = $login;
        $this->passwordHash = $passwordHash;
        $this->roles = $roles;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    /**
     * @return array | string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}
