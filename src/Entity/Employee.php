<?php

namespace App\Entity;

use JMS\Serializer\Annotation as Serializer;

class Employee
{
    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $name;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $position;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $city;

    /**
     * @Serializer\SerializedName("n")
     * @Serializer\Type("int")
     *
     * @var int
     */
    private $number;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $date;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     */
    private $salary;

    /**
     * @param string $name
     * @param string $position
     * @param string $city
     * @param int    $number
     * @param string $date
     * @param string $salary
     */
    public function __construct(
        string $name,
        string $position,
        string $city,
        int $number,
        string $date,
        string $salary
    ) {
        $this->name = $name;
        $this->position = $position;
        $this->city = $city;
        $this->number = $number;
        $this->date = $date;
        $this->salary = $salary;
    }

    /**
     * @param string $name
     * @param string $position
     * @param string $city
     * @param string $date
     * @param string $salary
     */
    public function edit(
        string $name,
        string $position,
        string $city,
        string $date,
        string $salary
    ): void {
        $this->name = $name;
        $this->position = $position;
        $this->city = $city;
        $this->date = $date;
        $this->salary = $salary;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }
}
